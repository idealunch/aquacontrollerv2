/*
 * AquaHTTP.h
 *
 *  Created on: Oct 31, 2019
 *      Author: doc
 */

#pragma once

#include "Arduino.h"
#include <AquaHTTP.h>
#include <AquaHelper.h>
#include <ESP8266httpUpdate.h>
#include <ESP8266HTTPUpdateServer.h>
#include <ESP8266HTTPClient.h>
#include <ESP8266WebServer.h>

class AquaHTTP{
public:
	void Init(Dictionary &responseCache);
	void HandleClient();
private:
};

void HttpSendDeviceJson();
void HttpSendCanalJson();
void HttpSendDayJson();
void HttpSendHourJson();
void HttpSendSecJson();
void HttpSendTempJson();
void HttpSendSensorJson();
void HttpSendPhJson();
void HttpSendStatsJson();

