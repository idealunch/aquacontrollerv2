#ifdef __IN_ECLIPSE__
//This is a automatic generated file
//Please do not modify this file
//If you touch this file your change will be overwritten during the next build
//This file has been generated on 2019-12-19 00:18:30

#include "Arduino.h"
#include <AquaTimers.h>
#include <AquaTemp.h>
#include <AquaHelper.h>
#include <AquaStop.h>
#include <AquaCanal.h>
#include <AquaEEPROM.h>
#include <AquaAnalog.h>
#include <AquaWiFi.h>

void setup() ;
void loop() ;
void ChangeChanalState() ;
void ChangeWaterLevelStatus(bool warning, byte canal) ;
void ChangeWiFiLog(String log) ;
void GetUDPWiFiPOSTRequest(typeResponse type, String json) ;
void SetPHSensorConfig(bool ph_686, byte sensor);
uint16_t SaveUTCSetting(uint16_t utc) ;

#include "AquaController2_0.ino"


#endif
